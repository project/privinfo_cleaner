Private Info Cleaner
--------------------

This module resolve the typical problem of sharing a production Drupal database
into a development environments with critical information as the privete data
of users, clients, orders, etc. That we need to protect. Whit this feature you
can configure a list of fields that you want to clean the private info and that
data will be replaced with same length random strings. With this approach you
could share your database with an outsourcing teams or freelancers that are new
at your team without worry about empty all the tables by hand.

Real data in the tables sometimes is very important for some kind of development
as creating views, search features, administration tasks, rules, bulk
operations, or load stress test when we need to reproduce queries with the real
volume of data. So instead of wasting time adding content to simulate what we
need we could safetely clone production DB and be safe of the private data.

